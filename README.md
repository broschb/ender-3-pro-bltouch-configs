# README #

Configs for building Marlin bugfix-2.0.x branch for the Ender 3 pro and BLTouch auto bed leveling sensor.

Download the Marlin bugfix-2.0.x branch
https://github.com/MarlinFirmware/Marlin/tree/bugfix-2.0.x

Follow instructions to setup vscode build env and use the configs attached changing desired settings and build.

assumes offsets for this mount(https://www.thingiverse.com/thing:3148733), change NOZZLE_TO_PROBE_OFFSET value to appropriate offset for your mount.

Add bin file to sdcard and place in Ender 3 and startup.

Win!!